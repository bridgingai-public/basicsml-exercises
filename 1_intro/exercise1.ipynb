{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![BridgingAI Logo](../bridgingai_logo.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Basics of Machine Learning - Excercise 1\n",
    "\n",
    "Welcome to the programming exercises for _Basics of Machine Learning_!\n",
    "These notebooks are not graded, but they give you the opportunity to practice what you have learned in the lectures.\n",
    "We will provide sample solutions for all exercises, but we highly recommend trying to solve them yourself first.\n",
    "This way, you strengthen your Python skills, improve your understanding of the topics - and of course it is very rewarding to see your code working!\n",
    "\n",
    "This week, we only make sure your environment is working and recap basic Numpy functionality, in particular arrays and broadcasting - play around with the code to get used to it!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Q1: Environment Check\n",
    "Let's start by checking your environment.\n",
    "Run the cell below; if your environment is set up properly, you should see a 3D plot of a function with two local maxima. This is an example of the probability density distribution of a Gaussian Mixture Model, one of the methods that you will learn about later in the course!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import sklearn\n",
    "from PIL import Image\n",
    "from matplotlib import pyplot as plt, cm\n",
    "from mpl_toolkits.mplot3d import Axes3D\n",
    "from scipy.stats import multivariate_normal\n",
    "\n",
    "# Gaussian mixture parameters\n",
    "mu1, sigma1 = [1, 1], [[1, 0.5], [0.5, 1]]\n",
    "mu2, sigma2 = [-1, -1], [[1, -0.7], [-0.7, 1]]\n",
    "\n",
    "# Create grid and multivariate normal\n",
    "x = np.linspace(-3, 3, 100)\n",
    "y = np.linspace(-3, 3, 100)\n",
    "X, Y = np.meshgrid(x, y)\n",
    "pos = np.stack([X, Y], axis=-1)\n",
    "\n",
    "# Create an object to calculate the density of each distribution\n",
    "rv1 = multivariate_normal(mu1, sigma1)\n",
    "rv2 = multivariate_normal(mu2, sigma2)\n",
    "\n",
    "# Compute the mixture's probability density function\n",
    "pdf = 0.5 * rv1.pdf(pos) + 0.5 * rv2.pdf(pos)\n",
    "\n",
    "# Make a 3D plot\n",
    "ax = plt.axes(projection=\"3d\")\n",
    "surf = ax.plot_surface(X, Y, pdf, facecolors=cm.viridis(pdf / pdf.max()))\n",
    "# transparent facecolors make it a wireframe plot\n",
    "surf.set_facecolor((0, 0, 0, 0))\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Q2: Numpy Recap\n",
    "In the programming exercises, we will make heavy use of [Numpy](https://numpy.org/doc/stable/).\n",
    "To quote the documentation:\n",
    "> NumPy is the fundamental package for scientific computing in Python. It is a Python library that provides a multidimensional array object, various derived objects (such as masked arrays and matrices), and an assortment of routines for fast operations on arrays, including mathematical, logical, shape manipulation, sorting, selecting, I/O, discrete Fourier transforms, basic linear algebra, basic statistical operations, random simulation and much more.\n",
    "\n",
    "Internally, Numpy implements most of the operators in C, not in Python, making them orders of magnitude faster than plain Python code.\n",
    "\n",
    "Let's recap the most basic functionality here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Q2.1: Arrays\n",
    "The basic building block of Numpy is the (multidimensional) array.\n",
    "Zero-dimensional arrays are effectively scalars, and you can think of 1D and 2D arrays as vectors and matrices, respectively.\n",
    "Going to higher dimensions, you may think of them as collections of matrices or matrices with multi-dimensional components."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can cast any list or tuple into a Numpy array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(np.array([[1, 2], [3, 4]]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When debugging, it is often helpful to inspect the shape of an array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create an array of the given shape filled with ones\n",
    "shape = (2, 3, 4)\n",
    "# create an array filled with ones\n",
    "a = np.ones(shape)\n",
    "print(a)\n",
    "print(a.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sometimes, we want to reshape an array, for example, to transpose a matrix, convert a grid into a list, or add unit dimensions for broadcasting (see below for examples of broadcasting)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# -1 lets numpy calculate the size of the axis\n",
    "b = np.arange(24).reshape(-1, 6)\n",
    "print(\"b:\", b.shape, \"\\n\", b)\n",
    "\n",
    "# indexing with a number yields the sub-array at that index\n",
    "print(\"b[1]:\", b[1].shape, \"\\n\", b[1])\n",
    "print(\"b[1, 2]:\", b[1, 2].shape, \"\\n\", b[1, 2])\n",
    "\n",
    "# you can use slices for indexing\n",
    "print(\"b[:, 1]\", b[:, 1].shape, \"\\n\", b[:, 1])\n",
    "\n",
    "# or an ellipsis as shorthand for keeping remaining dimensions\n",
    "print(\"b[..., -1]:\", b[..., -1].shape, \"\\n\", b[..., -1])\n",
    "\n",
    "# indexing with None introduces a unit dimension, which is useful for broadcasting\n",
    "print(\"b[None, ...]:\", b[None, ...].shape, \"\\n\", b[None, ...])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Most mathematical functions operate element-wise on arrays, that is, they are applied to each entry of the array individually."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print((b - 5) ** 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Reduction operations, such as `sum`, `max`, and `mean`, reduce the array to a scalar.\n",
    "However, sometimes we want to apply the reduction in parallel over some axis, for example, to compute the maximum entry of a list of vectors.\n",
    "We can use the `axis` keyword to specify the axis over which the reduction is applied, starting with the first (left-most) axis with `axis=0`.\n",
    "Negative values for `axis` count from the right."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(b.sum())\n",
    "print(b.sum(axis=0))\n",
    "print(b.sum(axis=-1))\n",
    "# sometimes it is useful to not squeeze the reduced dimensions\n",
    "print(b.sum(keepdims=True))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Matrix multiplication can be done with the `@` operator:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c = b.reshape(a.shape)\n",
    "# use array.T to inverse the dimensions\n",
    "print(b.shape, b.T.shape)\n",
    "# use array.transpose to arbitrarily shuffle dimensions\n",
    "print(c.transpose(0, 2, 1) @ (a + 1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Q2.2: Broadcasting\n",
    "A very convenient feature of Numpy arrays is [broadcasting](https://numpy.org/doc/stable/user/basics.broadcasting.html).\n",
    "Broadcasting tries to match the shapes of multiple arrays by introducing new axes or \"inflating\" unit dimensions and then applies the same operation over all created dimensions (you can find a more detailed description in the linked documentation page).\n",
    "This is much more efficient than writing a loop doing the same thing in Python.\n",
    "\n",
    "For example, consider applying a linear operation (i.e., multiplying a matrix) to a set of datapoints."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from time import time\n",
    "\n",
    "# lots of 8-dimensional datapoints\n",
    "a = np.ones((100000, 8))\n",
    "# the matrix representing the transformation\n",
    "t = np.ones((8, 8))\n",
    "\n",
    "start = time()\n",
    "out = np.zeros((a.shape[0], t.shape[0]))\n",
    "for i in range(a.shape[0]):\n",
    "    # Have to do some shenanigans to make the shapes match\n",
    "    out[i] = (t @ a[i, :, None]).squeeze()\n",
    "print(\"Python loop:\", time() - start, \"seconds\")\n",
    "\n",
    "start = time()\n",
    "# here we only add a unit dimension to view the data as 8x1 matrices\n",
    "out = t @ a[..., None]\n",
    "print(\"Broadcasting:\", time() - start, \"seconds\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Even in this simple example, the Numpy version is much faster.\n",
    "Additionally, it makes the code simpler by removing unneccessary control structures."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Q2.3: Random Numbers\n",
    "Sometimes, our algorithms make use of randomness, for example, drawing a random subset of data or generating samples according to some distribution.\n",
    "To this end, we can use Numpy's [random generator](https://numpy.org/doc/stable/reference/random/generator.html).\n",
    "\n",
    "The generator produces pseudo-random numbers - when initialized with the same seed, the sequence of numbers will be the same.\n",
    "When you run the cell below several times, you can observe that the seeded generator always produces the same samples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rng1 = np.random.default_rng(seed=42)\n",
    "rng2 = np.random.default_rng()\n",
    "\n",
    "mean, var = 0, 1\n",
    "shape = (100, 2)\n",
    "samples1 = rng1.normal(mean, var, shape)\n",
    "samples2 = rng2.normal(mean, var, shape)\n",
    "\n",
    "plt.scatter(*samples1.T, label=\"Fixed seed\")\n",
    "plt.scatter(*samples2.T, label=\"Random seed\")\n",
    "plt.legend(loc=\"upper left\")\n",
    "plt.ylim(-3, 3)\n",
    "plt.xlim(-3, 3)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can visualize the distributions using histograms, a technique that you will learn more about in the next chapter.\n",
    "For now, we just use the implementation available in matplotlib.\n",
    "\n",
    "Try different distributions and see what they look like; you can check the [documentation](https://numpy.org/doc/stable/reference/random/generator.html) to see which ones are available."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rng = np.random.default_rng()\n",
    "\n",
    "samples = rng.normal(size=10000)\n",
    "plt.hist(samples, density=True)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Warming up - Probability Theory Basics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Suppose that we have three colored boxes $r$ (red), $g$ (green), and $b$ (blue).\n",
    "Box $r$ contains 3 apples, 4 oranges, and 3 limes, box $g$ contains 3 apples, 3 oranges, and 4 limes, and box $b$ contains 1 apple, 1 orange, and 0 limes.\n",
    "A box is chosen at random with probabilities $p(r) = 0.2$, $p(g) = 0.6$, $p(b) = 0.2$, and a piece of fruit is removed from the box (with equal\n",
    "probability of selecting any of the items in the box)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Important Statistical Formulas\n",
    "\n",
    "Rules of probability:\n",
    "    $$\n",
    "    \\begin{aligned}\n",
    "        p(x) = \\sum_y p(x, y)\\\\\n",
    "        p(x, y) = p(y|x)p(x)\\\\\n",
    "    \\end{aligned}\n",
    "    $$\n",
    "\n",
    "Bayes rule:\n",
    "    $$\n",
    "    \\begin{aligned}\n",
    "        p(y|x) = \\frac{p(x|y)p(y)}{p(x)}\\\\\n",
    "\t\t= \\frac{p(x|y)p(y)}{\\sum_{y'}p(x|y')p(y')}\n",
    "\t\\end{aligned}\n",
    "    $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(a) What is the probability of selecting an apple?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(b) If we observe that the selected fruit is in fact an orange, what is the probability that\n",
    "it came from the green box?"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "384px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

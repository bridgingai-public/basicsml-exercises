# Basics of Machine Learning - Exercises

This repository contains the jupyter notebooks for the Mooc __Basics of Machine Learning__.
To set up your environment, you can use (for example) [conda](https://github.com/conda/conda), a free package manager.
We recommend to install it via [miniforge](https://conda-forge.org/download/); you can use it to create an environment with `conda env create -f environment.yaml`.
To activate this environment, run `conda activate basicsml`.

